package accountSystem;

import java.time.LocalDateTime;

public class AccountsDatabase {
	private SavingsAccount sa[];
	private CheckingAccount ca[];

	public AccountsDatabase() {
		ca = new CheckingAccount[3];
		sa = new SavingsAccount[2];

		ca[0] = new CheckingAccount("Julia Schimidt", 2343.22f, 1, 1);
		ca[0].setStrategy(new PremiumAccountStrategy());

		ca[1] = new CheckingAccount("Brian Mcguil", 33413.22f, 2, 2);
		ca[1].setStrategy(new BasicAccountStrategy());

		ca[2] = new CheckingAccount("Walter White", 32432.22f, 3, 3);
		ca[2].setStrategy(new StudentAccountStrategy());

		sa[0] = new SavingsAccount("Gustavo Fring", 12131.22f, 4, 4, LocalDateTime.now());

		sa[1] = new SavingsAccount("Hank Schreider", 167878.22f, 5, 5, LocalDateTime.now());

	}

	public Account search(int num) {
		int i;
		for (i = 0; i < sa.length; i++) {
			if (sa[i].getAccountNumber() == num) {
				return sa[i];
			}
		}
		for (i = 0; i < ca.length; i++) {
			if (ca[i].getAccountNumber() == num) {
				return ca[i];
			}
		}
		return null;
	}
}
