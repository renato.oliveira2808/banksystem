package accountSystem;

public abstract class Account {

	public static int ACTIVE = 1;
	public static int CLOSED = 2;
	public static int MAXOPS = 100;

	private int state;
	private String name;
	private int accountNumber;
	private int pwd;
	private float previous_balance;
	private String latest_ops[];
	private float latest_ops_values[];
	private int lastOp;
	private float balance;
	private float sumFees;

	public Account(String name, float balance, int accountNumber, int pwd) {
		this.state = Account.ACTIVE;
		this.name = name;
		this.balance = balance;
		this.accountNumber = accountNumber;
		this.pwd = pwd;
		this.lastOp = 0;
		this.latest_ops = new String[Account.MAXOPS];
		this.latest_ops_values = new float[Account.MAXOPS];
		this.latest_ops[lastOp] = "Create Account";
		this.latest_ops_values[lastOp] = 0;
	}

	private boolean isActive() {
		return state == Account.ACTIVE;
	}

	public void getBalance(int pwd) {
		if (!this.isActive()) {
			System.out.println("Closed Account");
		} else if (this.pwd != pwd) {
			System.out.println("Wrong password!");
		} else
			System.out.println("Your balance is: " + this.balance);
	}

	/*
	 * @param op
	 * 
	 * @param val
	 * 
	 * @param pwd
	 * 
	 * @return true
	 */
	public boolean debitValue(String op, float val, int pwd) {

		if (!this.isActive()) {
			System.out.println("Closed Account");
			return false;
		}

		if (this.pwd != pwd) {
			System.out.println("Wrong Password!");
			return false;
		}

		if (val < 0 || val > this.balance) {
			System.out.println("Insufficient balance: " + balance);
			return false;
		}

		if (lastOp == (Account.MAXOPS - 1))
			for (int i = 0; i < (Account.MAXOPS - 1); i++) {
				latest_ops[i] = latest_ops[i + 1];
				latest_ops_values[i] = latest_ops_values[i + 1];
			}
		else
			this.lastOp++;

		this.latest_ops[lastOp] = op;
		this.latest_ops_values[lastOp] = -val;
		this.previous_balance = this.balance;
		this.balance -= val;

		if (this.balance == 0) {
			state = Account.CLOSED;
			System.out.println("Account " + this.name + ", number " + this.accountNumber + " closed.");
		}
		return true;
	}

	public boolean creditValue(String op, float val) {
		if (!this.isActive()) {
			System.out.println("Closed Account");
			return false;
		}
		this.lastOp++;
		this.latest_ops[lastOp] = op;
		this.latest_ops_values[lastOp] = val;
		this.previous_balance = this.balance;
		this.balance += val;
		return true;
	}

	public float getSumFees() {
		return sumFees;
	}

	public void chargeFee(float fee) {
		this.previous_balance = this.balance;
		this.balance -= fee;
		this.sumFees += fee;
	}

	public int getAccountNumber() {
		return this.accountNumber;
	}

	public int getQtdOp() {
		return lastOp;
	}
}
