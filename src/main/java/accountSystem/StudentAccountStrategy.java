package accountSystem;

public class StudentAccountStrategy implements CheckingAccountStrategy {
	public static int INCLUDED_TRANSFERS = 5;
	public static int INCLUDED_WITHDRAWS = 12;
	public static float TRANSFER_FEE = 10.5f;
	public static float WITHDRAW_FEE = 5.5f;

	@Override
	public float getFee(String op) {
		if (op.contentEquals("transfer")) {
			return TRANSFER_FEE;
		} else if (op.contentEquals("withdraw")) {
			return WITHDRAW_FEE;
		} else
			return 0;
	}

	public int getIncludedTransfers() {
		return INCLUDED_TRANSFERS;
	}

	public int getIncludedWithdraws() {
		return INCLUDED_WITHDRAWS;
	}
}
