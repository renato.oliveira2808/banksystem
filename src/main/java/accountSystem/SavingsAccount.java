package accountSystem;

import java.sql.Date;
import java.time.LocalDateTime;

public class SavingsAccount extends Account {
	public static float TRANSFER_FEE = 7.2f;
	public static float WITHDRAW_FEE = 2.2f;
	private static LocalDateTime DATE_CREATED;

	public SavingsAccount(String name, float balance, int accountNumber, int pwd, LocalDateTime d) {
		super(name, balance, accountNumber, pwd);
		this.DATE_CREATED = d;
		// TODO Auto-generated constructor stub
	}

}
