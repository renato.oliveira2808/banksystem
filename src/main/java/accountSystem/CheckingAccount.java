package accountSystem;

public class CheckingAccount extends Account {

	public CheckingAccount(String name, float balance, int accountNumber, int pwd) {
		super(name, balance, accountNumber, pwd);
	}

	private CheckingAccountStrategy strategy;

	public void setStrategy(CheckingAccountStrategy strategy) {
		this.strategy = strategy;
	}

	public CheckingAccountStrategy getStrategy() {
		return this.strategy;
	}

}
