package accountSystem;

public interface CheckingAccountStrategy {

	public float getFee(String op);

	public int getIncludedTransfers();

	public int getIncludedWithdraws();
}
