package accountSystem;

public class AccountController {

	private AccountsDatabase db;

	public AccountController() {
		db = new AccountsDatabase();
	}

	public void getBalance(int num, int pwd) {
		Account c = db.search(num);
		if (c == null) {
			System.out.println("Invalid Account");
		} else
			c.getBalance(pwd);
	}

	public boolean withdraw(int num, int pwd, int val) {
		String op = "withdraw";
		Account c = db.search(num);
		if (c == null) {
			System.out.println("Invalid Account");
			return false;
		} else {
			if (c.debitValue(op, val, pwd)) {
				if (c instanceof SavingsAccount) {
					c.chargeFee(SavingsAccount.WITHDRAW_FEE);
				} else if (c instanceof CheckingAccount) {
					if (c.getQtdOp() > ((CheckingAccount) c).getStrategy().getIncludedWithdraws())
						c.chargeFee(((CheckingAccount) c).getStrategy().getFee(op));
				}
				return true;
			} else
				return false;
		}
	}

	public boolean deposit(int num, float val) {
		Account c = db.search(num);
		if (c != null) {
			return c.creditValue("deposit", val);
		} else {
			System.out.println("Invalid Account");
			return false;
		}
	}

	public boolean transfer(int num1, int num2, int pwd, float val) {
		String op = "transfer";
		Account c1 = db.search(num1);
		Account c2 = db.search(num2);
		if (c1 == null || c2 == null) {
			System.out.println("Invalid Account(s)");
			return false;
		} else {
			if (c1.debitValue(op, val, pwd) && c2.creditValue("transfer", val)) {
				if (c1 instanceof SavingsAccount) {
					c1.chargeFee(SavingsAccount.TRANSFER_FEE);
				} else if (c1 instanceof CheckingAccount) {
					if (c1.getQtdOp() > ((CheckingAccount) c1).getStrategy().getIncludedTransfers())
						c1.chargeFee(((CheckingAccount) c1).getStrategy().getFee(op));
				}
				return true;
			} else
				return false;
		}

	}

}
