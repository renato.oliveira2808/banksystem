package atmSystem;

import java.util.HashMap;
import java.util.Map;

public class Atm {
	private int balance;
	private int pwd;
	private int mode;
	Map<Integer, Integer> bills = null;

	public static int ADMIN_MODE = 0;
	public static int USER_MODE = 1;

	public Atm(int pwd) {
		this.pwd = pwd;
		this.balance = 0;
		this.bills = new HashMap<Integer, Integer>();
		this.empty(pwd);
		this.recharge(pwd, 100, 100, 100, 100);
	}

	public void changeMode() {
		if (this.mode == Atm.ADMIN_MODE)
			this.mode = Atm.USER_MODE;
		else
			System.out.println("Not Admin!");
	}

	public void changeMode(int pwd) {
		if (this.mode == Atm.USER_MODE)
			if (this.validatePwd(pwd))
				this.mode = Atm.ADMIN_MODE;
			else
				System.out.println("Wrong Password!");
	}

	public void releaseCash(int value) {
		int bills100 = 0, bills10 = 0, bills5 = 0, bills1 = 0, remain;

		if (value < 10) {
			System.out.println("Min Withdrwal is 10$");
		} else if (value > balance) {
			System.out.println("Insuficient Cash on ATM!");
		} else {
			if (value > 100) {
				bills100 = getBills(value, 100);
				remain = value - 100 * bills100;

				if (remain > 10) {
					bills10 = getBills(remain, 10);
					remain = remain - 10 * bills10;
					if (remain > 5) {
						bills5 = getBills(remain, 5);
						remain = remain - 5 * bills5;
						bills1 = getBills(remain, 1);
					}
					else
						bills1 = getBills(remain, 1);
				} else if (remain > 5) {
					bills5 = getBills(remain, 5);
					remain = -5 * bills5;
					bills1 = getBills(remain, 1);
				} else
					bills1 = getBills(remain, 1);
			} else {
				bills10 = getBills(value, 10);
				remain = value - 10 * bills10;
				if (remain > 5) {
					bills5 = getBills(remain, 5);
					remain = remain - 5 * bills5;
					bills1 = getBills(remain, 1);
				} else
					bills1 = getBills(remain, 1);
			}
			balance = balance - value;

			System.out.println("Releasing Cash:");
			System.out.println(bills100 + " 100$ bill(s)");
			System.out.println(bills10 + " 10$ bill(s)");
			System.out.println(bills5 + " 5$ bill(s)");
			System.out.println(bills1 + " 1$ bill(s)");
		}
	}

	private int getBills(int value, int bill_value) {
		int n = value / bill_value;
		if (bills.get(bill_value) - n > 0) {
			bills.replace(bill_value, bills.get(bill_value) - n);
			return n;
		} else
			return bills.get(bill_value);
	}

	public void printAvailableBills() {
		System.out.println("==== AVAILABLE BILLS ====");
		System.out.println(bills.get(1) + " 1$ bill(s) available");
		System.out.println(bills.get(5) + " 5$ bill(s) available");
		System.out.println(bills.get(10) + " 10$ bill(s) available");
		System.out.println(bills.get(100) + " 100$ bill(s) available");
		System.out.println("TOTAL AMOUNT AVAILABLE: " + this.balance + "$");
		System.out.println("");
	}

	public void recharge(int pwd, int bills1, int bills5, int bills10, int bills100) {
		if (this.validatePwd(pwd)) {
			bills.put(1, bills.get(1) + bills1);
			bills.put(5, bills.get(5) + bills5);
			bills.put(10, bills.get(10) + bills10);
			bills.put(100, bills.get(100) + bills100);

			this.balance = bills.get(1) + bills.get(5) * 5 + bills.get(10) * 10 + bills.get(100) * 100;
		} else
			System.out.println("Wrong Password!");
	}

	public void empty(int pwd) {
		if (this.validatePwd(pwd)) {
			bills.put(1, 0);
			bills.put(5, 0);
			bills.put(10, 0);
			bills.put(100, 0);

			this.balance = 0;
		} else
			System.out.println("Wrong Password!");
	}

	public boolean validatePwd(int pwd) {
		return this.pwd == pwd;
	}

	public void changePwd(int pwd_new, int pwd_old) {
		if (this.pwd == pwd_old && this.mode == Atm.ADMIN_MODE)
			this.pwd = pwd_new;
		else
			System.out.println("Wrong Password!");
	}

	public int getMode() {
		return mode;
	}

	public float getBalance() {
		return balance;
	}
}
