package atmSystem;

import accountSystem.AccountController;

public class AtmController {
	private Atm atm;
	private AccountController c;

	public AtmController(int pwd) {
		atm = new Atm(pwd);
		c = new AccountController();
	}

	public void getAccountBalance(int num, int pwd) {
		c.getBalance(num, pwd);
	}

	public void withdrawAccount(int num, int pwd, int val) {
		if (c.withdraw(num, pwd, val)) {
			this.atm.releaseCash(val);
		}
	}

	public boolean depositAmount(int num, float val) {
		return c.deposit(num, val);
	}

	public boolean transferAmount(int num1, int num2, int pwd, float val) {
		return c.transfer(num1, num2, pwd, val);
	}

	public void recharge(int pwd, int bills1, int bills5, int bills10, int bills100) {
		this.atm.recharge(pwd, bills1, bills5, bills10, bills100);
	}

	public void empty(int pwd) {
		this.atm.empty(pwd);
	}

	public void showAvailableBills() {
		this.atm.printAvailableBills();
	}

	public void changeMode() {
		this.atm.changeMode();
	}

	public void changeMode(int pwd) {
		this.atm.changeMode(pwd);
	}

	public int getMode() {
		return this.atm.getMode();
	}

	public void changeAtmPwd(int pwd_new, int pwd_old) {
		this.atm.changePwd(pwd_new, pwd_old);
	}

}