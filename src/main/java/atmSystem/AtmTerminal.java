package atmSystem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AtmTerminal {
	private AtmController atmController;
	private int op;
	InputStreamReader r;
	BufferedReader br;

	public AtmTerminal(int pwd) {
		r = new InputStreamReader(System.in);
		br = new BufferedReader(r);
		atmController = new AtmController(pwd);
		op = -1;
	}

	public void showMenu() {
		while (op < 0 || op > 6) {
			if (atmController.getMode() == Atm.USER_MODE) {
				System.out.println("======= USER MODE =======");
				System.out.println("Please choose the Operation");
				System.out.println("0 - Back to Menu");
				System.out.println("1 - Get Account Balance ");
				System.out.println("2 - Deposit");
				System.out.println("3 - Withdraw");
				System.out.println("4 - Transfer");
				System.out.println("5 - Change Mode");
				System.out.println("6 - Exit");

				try {
					op = Integer.parseInt(br.readLine());
					doOperation();
				} catch (Exception e) {
					e.printStackTrace();
					op = -1;
				}
			} else if (atmController.getMode() == Atm.ADMIN_MODE) {
				System.out.println("======= ADMIN MODE =======");
				System.out.println("Please choose the Operation");
				System.out.println("0 - Back to Menu");
				System.out.println("1 - Check Available Bills");
				System.out.println("2 - Recharge");
				System.out.println("3 - Change ATM Password");
				System.out.println("4 - Empty ATM");
				System.out.println("5 - Change Mode");
				System.out.println("6 - Exit");

				try {
					op = Integer.parseInt(br.readLine());
					doOperation();
				} catch (Exception e) {
					System.out.println("Error reading input. Please try again");
					op = -1;
				}
			}
		}
	}

	public void doOperation() {
		InputStreamReader r = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(r);
		int num, pwd, bills1, bills5, bills10, bills100;
		float val;
		if (op == 6) {
			System.out.println("Exittting");
			close();
		} else {
			if (atmController.getMode() == Atm.USER_MODE) {
				if (op == 5) {
					System.out.println("Please enter the ATM password:");
					try {
						atmController.changeMode(Integer.parseInt(br.readLine()));
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					switch (op) {
					case 1:
						try {
							System.out.println("Please enter your Account Number:");
							num = Integer.parseInt(br.readLine());
							System.out.println("Please enter your Password:");
							pwd = Integer.parseInt(br.readLine());
							atmController.getAccountBalance(num, pwd);
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case 2:
						try {
							System.out.println("Please enter your Account Number:");
							num = Integer.parseInt(br.readLine());
							System.out.println("Please enter the Amount:");
							val = Float.parseFloat(br.readLine());
							if (atmController.depositAmount(num, val)) {
								System.out.println("DEPOSIT SUCCESSFUL");
							} else {
								System.out.println("DEPOSIT FAILED");
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case 3:
						try {
							System.out.println("Please enter your Account Number:");
							num = Integer.parseInt(br.readLine());
							System.out.println("Please enter your Password:");
							pwd = Integer.parseInt(br.readLine());
							atmController.showAvailableBills();
							System.out.println("Please enter the Amount:");
							atmController.withdrawAccount(num, pwd, Integer.parseInt(br.readLine()));
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case 4:
						try {
							System.out.println("Please enter your Account Number:");
							num = Integer.parseInt(br.readLine());
							System.out.println("Please enter your Password:");
							pwd = br.read();
							System.out.println("Please enter the Amount:");
							val = Float.parseFloat(br.readLine());
							System.out.println("Please enter account to be credited:");
							atmController.transferAmount(num, br.read(), pwd, val);
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					default:
						System.out.println("Invalid Operation");
					}
				}
			} else {
				if (op == 5) {
					atmController.changeMode();
				} else {
					switch (op) {
					case 1:
						atmController.showAvailableBills();
						break;
					case 2:
						try {
							System.out.println("Please enter the ATM Password:");
							pwd = Integer.parseInt(br.readLine());
							System.out.println(
									"Please enter number of 1$ 5$ 10$ and 100$ bills in that order, each followed by Enter:");
							bills1 = Integer.parseInt(br.readLine());
							;
							bills5 = Integer.parseInt(br.readLine());
							bills10 = Integer.parseInt(br.readLine());
							bills100 = Integer.parseInt(br.readLine());
							atmController.recharge(pwd, bills1, bills5, bills10, bills100);
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case 3:
						try {
							System.out.println("Please enter the ATM Password:");
							pwd = Integer.parseInt(br.readLine());
							System.out.println("Please enter the New ATM Password:");
							atmController.changeAtmPwd(Integer.parseInt(br.readLine()), pwd);
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case 4:
						try {
							System.out.println("Please enter the ATM Password:");
							pwd = Integer.parseInt(br.readLine());
							atmController.empty(pwd);
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					default:
						System.out.println("Invalid Operation");
					}
				}
			}
			op = -1;
			System.out.println("");
			showMenu();
		}
	}

	public void close() {
		try {
			r.close();
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
